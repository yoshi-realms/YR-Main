package tk.yoshirealms.Listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import tk.yoshirealms.yrmain;

import static org.bukkit.Bukkit.getServer;

/**
 * Created by Asher on 11/07/2017.
 * Project name: YR-Main
 */
public class PlayerJoinListener implements Listener {
    @EventHandler
    public void PlayerJoinEvent(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (yrmain.NoStandardJoinMSG) {
            e.setJoinMessage(null);
        }
        if (p.hasPermission("yoshirealms.announcejoin") && yrmain.AnnounceJoinOn) {
            getServer().broadcastMessage(yrmain.JoinMessage.replaceAll("<PlayerName>", p.getName().replaceAll("<ServerName>", yrmain.ServerName)));
        }
        if (yrmain.LoginMessageOn) {
            /*p.sendTitle(main.Title, main.subTitle.replaceAll("<PlayerName>", p.getName()), main.FadeIn * 20, main.Stay * 20, main.FadeOut * 20);*/
            /*Bukkit.getServer().dispatchCommand(getServer().getConsoleSender(), "title" + " " + p.getName() + " " + "times" + " " + main.FadeIn + " " + main.Stay + " " + main.FadeOut);*/

            /*in 1.8 moet het TitleAPI.nogwattes zijn*/


            if (yrmain.LoginMessage != null) {
                for (String str : yrmain.LoginMessage) {
                    p.sendMessage(str.replaceAll("&", "§").replaceAll("<PlayerName>", p.getName()).replaceAll("<ServerName>", yrmain.ServerName));
                }
            }
        }

    }
    @EventHandler
    public void PlayerLeaveEvent(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (yrmain.NoStandardJoinMSG) {
            e.setQuitMessage(null);
        }
        if (p.hasPermission("yoshirealms.announceleave") && yrmain.AnnounceLeaveOn) {
            getServer().broadcastMessage(yrmain.LeaveMessage.replaceAll("<PlayerName>", p.getName().replaceAll("<ServerName>", yrmain.ServerName)));
        }
    }
}
