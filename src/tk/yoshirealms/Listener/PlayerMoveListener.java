package tk.yoshirealms.Listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 02/01/2017.
 */
public class PlayerMoveListener implements Listener {
    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent Event) {
        Player player = Event.getPlayer();
        if (player.getLocation().getBlockY() ==1 && yrmain.VoidKill) {
            player.setHealth(0.0);
            if (yrmain.InstantRespawn) {
                player.spigot().respawn();
            }
        }
        if (player.getLocation().getBlockY() ==-10 && yrmain.VoidKill) {
            player.setHealth(0.0);
            if (yrmain.InstantRespawn) {
                player.spigot().respawn();
            }
        }
        if (player.getLocation().getBlockY() ==-40 && yrmain.VoidKill) {
            player.setHealth(0.0);
            if (yrmain.InstantRespawn) {
                player.spigot().respawn();
            }
        }
    }
}