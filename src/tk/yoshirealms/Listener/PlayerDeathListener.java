package tk.yoshirealms.Listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 07/01/2017.
 */
public class PlayerDeathListener implements Listener {
    @EventHandler
    public void onPlayerDeathEvent (PlayerDeathEvent event) {
        Player player = event.getEntity();
        if (player.getHealth() ==0.0 && yrmain.InstantRespawn) {
            Bukkit.getScheduler().runTaskLater(yrmain.plugin, () -> {
                player.spigot().respawn();
            },2 );
        }
    }
}
