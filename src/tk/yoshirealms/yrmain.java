package tk.yoshirealms;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import tk.yoshirealms.Commands.*;
import tk.yoshirealms.Listener.PlayerDeathListener;
import tk.yoshirealms.Listener.PlayerJoinListener;
import tk.yoshirealms.Listener.PlayerMoveListener;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Created by Asher on 02/01/2017.
 * Project name: YR-Main
 *
 * 	for (Player player : Bukkit.getServer().getOnlinePlayers()) {
 if (player.isOp()) {
 //send your message
 }
 }
 *
 */
public class yrmain extends JavaPlugin
{
    public static Long serverStartTime;
    public static List<String> DiscordMessage , PluginsCmd , CustomHelp , GCMessage , LoginMessage;
    private static Configuration config;
    public static yrmain plugin;
    public static String SpawnWorld = "World";
    public static FileConfiguration Storage , KitSafe;
    public static File StorageFile , KitSafeFile;
    public static int TPCooldown , FadeIn , FadeOut , Stay;
    public static String TPCmessage , HelpMsg , FeedMsg, HealMsg , DayMsg , NightMsg , TpDefineP , TpMsg , GmMsg , HasteToff , HasteTon , HealRe , FeedRe , FlyRe , GCWorldLine , GmRe , NoPerms , BCPrefix , ReloadMessage , NVon , NVoff , SetSpawn , FlyMsg , TPMessage , GmMsg2 , FeedMsg2 , HealMsg2 , FlyMsg2 , NoAfk , OnAfk , InvseeDefine , ServerName , Title , subTitle , JoinMessage , LeaveMessage , WarpSucces , SetWarpSucces , tWarpSucces , NoWarps , AvailableWarps , DelWarpSucces , TPCancelled;
    public static boolean SpawnOn , NightVOn , BroadCastOn , PluginsOn , DiscordOn , SetSpawnOn , FeedOn , HealOn , FlyOn , DayOn , NightOn , STpOn , GmOn , HasteOn , GCOn , VoidKill , InstantRespawn , AfkOn , CraftOn , EcOn , InvseeOn , RepairOn , AnvilOn , LoginMessageOn , NoStandardJoinMSG , AnnounceJoinOn , AnnounceLeaveOn , SetWarpOn , WarpOn , WarpsOn , DelWarpOn;

    @Override
    public void onEnable() {
        config = this.getConfig();
        plugin = this;
        StorageFile = new File(plugin.getDataFolder() , "Storage.yml");
        Storage = YamlConfiguration.loadConfiguration(StorageFile);
        KitSafeFile = new File(plugin.getDataFolder() , "Kits");
        KitSafe = YamlConfiguration.loadConfiguration(KitSafeFile);

        load();
        saveDefaultConfig();

        serverStartTime = System.currentTimeMillis();

        this.getCommand("discord").setExecutor(new commandDiscord());

        getServer().getPluginManager().registerEvents(new PlayerMoveListener(),this);
        getServer().getPluginManager().registerEvents(new PlayerDeathListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);

        this.getCommand("plugin").setExecutor(new commandPlugins());
        this.getCommand("pluginslist").setExecutor(new commandPlugins());
        this.getCommand("pluginlist").setExecutor(new commandPlugins());
        this.getCommand("plugins-list").setExecutor(new commandPlugins());
        this.getCommand("plugin-list").setExecutor(new commandPlugins());

        this.getCommand("yell").setExecutor(new commandBroodkast());

        this.getCommand("nv").setExecutor(new commandNightV());

        this.getCommand("yr").setExecutor(new commandYR());

        this.getCommand("spawn").setExecutor(new commandSpawn());
        this.getCommand("setspawn").setExecutor(new commandSetSpawn());

        this.getCommand("heal").setExecutor(new commandHeal());
        this.getCommand("feed").setExecutor(new commandFeed());

        this.getCommand("fly").setExecutor(new commandFly());

        this.getCommand("day").setExecutor(new commandDay());
        this.getCommand("night").setExecutor(new commandNight());

        this.getCommand("stp").setExecutor(new commandSTp());

        this.getCommand("gm").setExecutor(new commandGM());

        this.getCommand("haste").setExecutor(new commandHaste());

        this.getCommand("gc").setExecutor(new commandGC());

        this.getCommand("afk").setExecutor(new commandAFK());

        this.getCommand("craft").setExecutor(new commandCraft());

        this.getCommand("ec").setExecutor(new commandEc());

        this.getCommand("invsee").setExecutor(new commandInvSee());

        this.getCommand("repair").setExecutor(new commandRepair());

        this.getCommand("anvil").setExecutor(new commandAnvil());

        this.getCommand("warp").setExecutor(new commandWarp());
        this.getCommand("setwarp").setExecutor(new commandSetWarp());
        this.getCommand("warps").setExecutor(new commandWarps());
        this.getCommand("delwarp").setExecutor(new commandDelWarp());

        for (String str : Storage.getStringList("Warps")) {
            String[] strs = str.split(":");
            String warpName = strs[1];
            String[] strss = strs[0].split(";");

            int x = Integer.parseInt(strss[0]);
            int y = Integer.parseInt(strss[1]);
            int z = Integer.parseInt(strss[2]);
            World world = Bukkit.getServer().getWorld(strss[3]);
            Location loc = new Location(world,x , y, z);
            commandSetWarp.Warps.put(warpName , loc);

        }
    }
    public void onDisable() {
        save();
    }
    private void save() {
        Storage.set("Spawn-World" , SpawnWorld);
        try {
            Storage.save(StorageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            KitSafe.save(KitSafeFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void load() {
        plugin.reloadConfig();
        GCOn = config.getBoolean("/gc");
        GCMessage = config.getStringList("GC-Message");
        GCWorldLine = config.getString("GC-World-Line");
        InstantRespawn = config.getBoolean("InstantRespawn");
        NoPerms = config.getString("No-Permissions").replaceAll("&","§");
        BCPrefix = config.getString("Prefix").replaceAll("&","§");
        VoidKill = config.getBoolean("Voidkill");
        DiscordMessage = config.getStringList("Discord-Message");
        CustomHelp = config.getStringList("HelpMessage");
        PluginsCmd = config.getStringList("Fake-Plugins");
        ReloadMessage = config.getString("Reload-Message").replaceAll("&","§");
        NVon = config.getString("NightVisionOnMessage").replaceAll("&","§");
        NVoff = config.getString("NightVisionOffMessage").replaceAll("&","§");
        SpawnWorld = Storage.getString("Spawn-World");
        TPMessage = config.getString("Teleport-Message").replaceAll("&","§");
        SetSpawn = config.getString("SetSpawn-Message").replaceAll("&","§");
        TPCooldown = config.getInt("Teleport-Cooldown");
        TPCmessage = config.getString("TeleportCooldown-Message").replaceAll("<Cooldown>" , Integer.toString(yrmain.TPCooldown)).replaceAll("&","§");
        DiscordOn = config.getBoolean("/discord");
        PluginsOn = config.getBoolean("/plugin");
        BroadCastOn = config.getBoolean("/yell");
        NightVOn = config.getBoolean("/nv");
        SpawnOn = config.getBoolean("/spawn");
        SetSpawnOn = config.getBoolean("/setspawn");
        HelpMsg = config.getString("Command turned off").replaceAll("&","§");
        HealOn = config.getBoolean("/heal");
        FeedOn = config.getBoolean("/feed");
        HealMsg = config.getString("/heal Message").replaceAll("&","§");
        FeedMsg = config.getString("/feed Message").replaceAll("&","§");
        FlyOn = config.getBoolean("/fly");
        FlyMsg = config.getString("/fly Message").replaceAll("&","§");
        DayOn = config.getBoolean("/day");
        DayMsg = config.getString("/day Message").replaceAll("&","§");
        NightOn = config.getBoolean("/night");
        NightMsg = config.getString("/night Message").replaceAll("&","§");
        STpOn = config.getBoolean("/stp");
        TpDefineP = config.getString("No player found to teleport").replaceAll("&","§");
        TpMsg = config.getString("/tp Message").replaceAll("&","§");
        GmOn = config.getBoolean("/gm");
        GmMsg = config.getString("/gm Message").replaceAll("&","§");
        HasteOn = config.getBoolean("/haste");
        HasteToff = config.getString("/haste off Message").replaceAll("&","§");
        HasteTon = config.getString("/haste on Message").replaceAll("&","§");
        HealRe = config.getString("/heal response Message").replaceAll("&","§");
        FlyRe = config.getString("/fly response Message").replaceAll("&","§");
        FeedRe = config.getString("/feed response Message").replaceAll("&","§");
        GmRe = config.getString("/gm response Message").replaceAll("&","§");
        GmMsg2 = config.getString("/gm Message by player").replaceAll("&","§");
        FeedMsg2 = config.getString("/feed Message by player").replaceAll("&","§");
        HealMsg2 = config.getString("/heal Message by player").replaceAll("&","§");
        FlyMsg2 = config.getString("/fly Message by player").replaceAll("&","§");
        AfkOn = config.getBoolean("/afk");
        NoAfk = config.getString("/afk Message no longer AFK").replaceAll("&","§");
        OnAfk = config.getString("/afk Message now AFK").replaceAll("&","§");
        CraftOn = config.getBoolean("/craft");
        EcOn = config.getBoolean("/ec");
        InvseeOn = config.getBoolean("/invsee");
        InvseeDefine = config.getString("No player found to invsee").replaceAll("&","§");
        RepairOn = config.getBoolean("/repair");
        AnvilOn = config.getBoolean("/anvil");
        LoginMessage = config.getStringList("Player Joined Message");
        ServerName = config.getString("Server name").replaceAll("&","§");
        LoginMessageOn = config.getBoolean("Player Joined Message On");
        NoStandardJoinMSG = config.getBoolean("Turn default join message off");
        Title = config.getString("Player Joined Title").replaceAll("&","§");
        subTitle = config.getString("Player Joined subTitle").replaceAll("&","§");
        Stay = config.getInt("Stay time (s)");
        FadeIn = config.getInt("Fade in time (s)");
        FadeOut = config.getInt("Fade out time (s)");
        AnnounceJoinOn = config.getBoolean("Turn custom join messages on");
        JoinMessage = config.getString("Custom join message for people with permission (yoshirealms.announcejoin)").replaceAll("&","§");
        AnnounceLeaveOn = config.getBoolean("Turn custom leave messages on");
        LeaveMessage = config.getString("Custom leave message for people with permission (yoshirealms.announceleave)").replaceAll("&","§");
        SetWarpOn = config.getBoolean("/setwarp");
        WarpOn = config.getBoolean("/warp");
        SetWarpSucces = config.getString("/setwarp Succes").replaceAll("&","§");
        WarpSucces = config.getString("/warp Succes").replaceAll("&","§");
        tWarpSucces = config.getString("/warp Succes on target").replaceAll("&","§");
        WarpsOn = config.getBoolean("/warps");
        NoWarps = config.getString("/warps Failure").replaceAll("&","§");
        AvailableWarps = config.getString("/warps Succes").replaceAll("&","§");
        DelWarpOn = config.getBoolean("/delwarp");
        DelWarpSucces = config.getString("/delwarp Succes").replaceAll("&","§");
        TPCancelled = config.getString("/tp Cancelled Message").replaceAll("&","§");
    }
}