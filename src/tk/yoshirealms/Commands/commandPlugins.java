package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 03/01/2017.
 */
public class commandPlugins implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player && yrmain.PluginsOn) {
            Player player = (Player)commandSender;
            if(yrmain.PluginsCmd != null) {
                for (String str : yrmain.PluginsCmd) {
                    player.sendMessage(str.replaceAll("&","§")); //Replace all & tokens with §
            }
            }
        }

        else if (!yrmain.NightVOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }

        return true;
    }
}
