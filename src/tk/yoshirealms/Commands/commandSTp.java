package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by decoo on 17/01/2017.
 */
public class commandSTp implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage("Command is only for players");
            return true;
        }
        Player p = (Player)commandSender;

        if (p.hasPermission("yoshirealms.stp") && yrmain.STpOn) {
            if (!(strings.length == 1)) {
                p.sendMessage(yrmain.BCPrefix + yrmain.TpDefineP);
                return true;
            }
            Player target = Bukkit.getServer().getPlayer(strings[0]);
            if (target == null) {
                p.sendMessage(yrmain.BCPrefix + "§cCould not find player§4 " + strings[0] + " §c!");
                return true;
            }
            p.teleport(target.getLocation());
            p.sendMessage(yrmain.BCPrefix + yrmain.TpMsg);
            return true;
        } else if (yrmain.STpOn) {
            p.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        }
        else if (!yrmain.STpOn) {
            p.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}
