package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 08/07/2017.
 * Project name: YR-Main
 */
public class commandInvSee implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.invsee") && yrmain.InvseeOn) {
            if (commandSender instanceof Player && strings.length > 0) {
                Player p = (Player)commandSender;
                OfflinePlayer target = Bukkit.getServer().getPlayer(strings[0]);
                Inventory inv;
                if (strings.length == 1) {
                    inv = target.getPlayer().getInventory();
                    p.closeInventory();
                    p.openInventory(inv);
                    return true;
                } else if (strings.length > 1){
                    inv = Bukkit.getServer().createInventory(Bukkit.getServer().getPlayer("CONSOLE"), 9, "Armor");
                    inv.setContents(target.getPlayer().getInventory().getArmorContents());
                    p.closeInventory();
                    p.openInventory(inv);
                    return true;
                }
            }
        } if (commandSender.hasPermission("yoshirealms.invsee") && yrmain.InvseeOn && strings.length == 0) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.InvseeDefine);
        } else if (!commandSender.hasPermission("yoshirealms.invsee") && yrmain.InvseeOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);

        } else if (!yrmain.InvseeOn) {
            commandSender.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}
