package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrmain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asher on 25/03/2017.
 * Project name: YR-Main
 */
public class commandAFK implements CommandExecutor {

    public static List<String> afkList = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender.hasPermission("yoshirealms.afk") && yrmain.AfkOn) {
            Player p = (Player) commandSender;
            if (afkList.contains(p.getName())) {
                afkList.remove(p.getName());
                Bukkit.broadcastMessage(yrmain.BCPrefix + yrmain.NoAfk.replaceAll("<PlayerName>" , commandSender.getName()));
                p.removePotionEffect(PotionEffectType.SLOW);
                p.removePotionEffect(PotionEffectType.BLINDNESS);
                p.removePotionEffect(PotionEffectType.JUMP);
            } else if (!afkList.contains(p.getName())) {
                afkList.add(p.getName());
                Bukkit.broadcastMessage(yrmain.BCPrefix + yrmain.OnAfk.replaceAll("<PlayerName>" , commandSender.getName()));
                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW , 1000000, 254, true, true));
                p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP , 1000000, 250, true, true));
                p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS , 1000000, 1, true, true));
            }
        }
        else if (yrmain.AfkOn) {
            Player p = (Player) commandSender;
            p.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        }
        else if (!yrmain.AfkOn) {
            Player p = (Player) commandSender;
            p.sendMessage(yrmain.BCPrefix + yrmain.HelpMsg);
        }
        return true;
    }
}