package tk.yoshirealms.Commands;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import tk.yoshirealms.yrmain;

import static tk.yoshirealms.Commands.commandSetWarp.Warps;
import static tk.yoshirealms.Commands.commandSetWarp.storeList;
import static tk.yoshirealms.yrmain.Storage;

/**
 * Created by Asher on 21/08/2017.
 * Project name: YR-Main
 */
public class commandDelWarp implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender p, Command command, String s, String[] args) {
            if (p.hasPermission("yoshirealms.delwarp") && yrmain.DelWarpOn) {
                if (args.length == 0 || args.length > 1) {
                    return false;
                }
                else if (args.length == 1) {
                    if (Warps.containsKey(args[0])) {
                        Warps.remove(args[0]);

                        //De real shit van unsaven
                        for (String warp : Warps.keySet()) {
                            Location loc = Warps.get(warp);
                            World world = loc.getWorld();
                            String store = (int) loc.getX() + ";" + (int) loc.getY() + ";" + (int) loc.getZ() + ";" + world.getName() + ":" + warp;
                            storeList.add(store);
                        }
                        Storage.set("Warps", storeList);
                        p.sendMessage(yrmain.BCPrefix + yrmain.DelWarpSucces.replaceAll("<WarpName>", args[0]));

                        return true;
                    }
                }
                else if (!p.hasPermission("yoshirealms.setwarp") && yrmain.SetWarpOn) {
                    p.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                    return true;
                }
                else if (!yrmain.SetWarpOn) {
                    p.sendMessage(yrmain.BCPrefix + yrmain.HelpMsg);
                    return true;
                }
            }
        return true;
    }
}
