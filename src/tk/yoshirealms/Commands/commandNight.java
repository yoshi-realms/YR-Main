package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 17/01/2017.
 */
public class commandNight implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.night") && yrmain.NightOn) {
            Player p = (Player) commandSender;
            p.getWorld().setTime(13000);
            p.sendMessage(yrmain.BCPrefix + yrmain.NightMsg);
        } else if (yrmain.NightOn) {
            Player p = (Player) commandSender;
            p.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        } else if (!yrmain.NightOn) {
            Player p = (Player) commandSender;
            p.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}
