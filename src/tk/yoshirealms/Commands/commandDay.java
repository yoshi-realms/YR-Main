package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 17/01/2017.
 */
public class commandDay implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.day") && yrmain.DayOn) {
            Player p = (Player) commandSender;
            p.getWorld().setTime(1000);
            p.sendMessage(yrmain.BCPrefix + yrmain.DayMsg);
        } else if (yrmain.DayOn) {
            Player p = (Player) commandSender;
            p.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        } else if (!yrmain.DayOn) {
            Player p = (Player) commandSender;
            p.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}
