package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 20/08/2017.
 * Project name: YR-Main
 */
public class commandWarp implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player p = (Player) commandSender;
            if (commandSender.hasPermission("yoshirealms.warp") && yrmain.WarpOn) {
                if (args.length == 0) {
                    return false;
                } else if (args.length == 1 && p.hasPermission("yoshirealms.warp" + "." + args[0])) {
                    if (commandSetWarp.Warps.containsKey(args[0])) {
                        Location warpLocation = commandSetWarp.Warps.get(args[0]);
                        p.teleport(warpLocation);
                        p.sendMessage(yrmain.BCPrefix + yrmain.WarpSucces.replaceAll("<WarpName>", args[0]));
                        return true;
                    } else {
                        return false;
                    }
                } else if (args.length == 2 && p.hasPermission("yoshirealms.warp" + "." + args[0])) {
                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (target.getName().equalsIgnoreCase(args[1]) && commandSetWarp.Warps.containsKey(args[0])) {
                            Location warpLocation = commandSetWarp.Warps.get(args[0]);
                            target.teleport(warpLocation);
                            target.sendMessage(yrmain.BCPrefix + yrmain.WarpSucces.replaceAll("<WarpName>", args[0]) + " " + "§aby" + " " + p.getName());
                            p.sendMessage(yrmain.BCPrefix + yrmain.tWarpSucces.replaceAll("<WarpName>", args[0]).replaceAll("<Target>", args[1]));
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            } else if (!commandSender.hasPermission("yoshirealms.warp") && yrmain.WarpOn) {
                p.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                return true;
            } else if (!yrmain.WarpOn) {
                p.sendMessage(yrmain.BCPrefix + yrmain.HelpMsg);
                return true;
            }
            return true;
        }
        return true;
    }
}
