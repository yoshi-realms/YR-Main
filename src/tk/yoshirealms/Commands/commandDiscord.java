package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 02/01/2017.
 */
public class commandDiscord implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player && yrmain.DiscordOn) {
            Player player = (Player)commandSender;
            if(yrmain.DiscordMessage != null) {
                for (String str : yrmain.DiscordMessage) {
                    player.sendMessage(str.replaceAll("&","§")); //Replace all & tokens with §
            }
            }
            }
        else if (!yrmain.DiscordOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }
        return true;
    }
}
