package tk.yoshirealms.Commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

import static tk.yoshirealms.yrmain.plugin;

/**
 * Created by Asher on 07/01/2017.
 */
public class commandYR implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length == 1) {
            if ("plugins".equalsIgnoreCase(strings[0])) {
                if (commandSender instanceof Player) {
                    commandSender.sendMessage("§f-------=======[§4§lYoshi§6§lRealms §fPlugins§f]=======-------");
                    if (Bukkit.getPluginManager().getPlugin("YR-Main") != null) {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Main §8[§aEnabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    } else {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Main §8[§cDisabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    }



                    if (Bukkit.getPluginManager().getPlugin("YR-Perks") != null) {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Perks §8[§aEnabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    } else {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Perks §8[§cDisabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    }



                    if (Bukkit.getPluginManager().getPlugin("YR-Craft") != null) {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Craft §8[§aEnabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    } else {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Craft §8[§cDisabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    }



                    if (Bukkit.getPluginManager().getPlugin("YR-Management") != null) {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Management §8[§aEnabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    } else {
                        Player player = (Player) commandSender;
                        TextComponent Message = new TextComponent("§fYR-Management §8[§cDisabled§8]");
                        Message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/yoshi-realms"));
                        Message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cClick here to download").create()));
                        player.spigot().sendMessage(Message);
                    }
                }
                return true;
            } else if ("reload".equalsIgnoreCase(strings[0])) {
                if (commandSender.hasPermission("yoshirealms.reload")) {
                    Player player = (Player) commandSender;
                    yrmain.load();
                    player.getServer().getPluginManager().disablePlugin(plugin);
                    player.getServer().getPluginManager().enablePlugin(plugin);
                    player.sendMessage(yrmain.BCPrefix + yrmain.ReloadMessage);
                    return true;
                } else {
                    commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                }
                return true;
            } else if ("help".equalsIgnoreCase(strings[0])) {
                Player player = (Player) commandSender;
                if (yrmain.CustomHelp != null) {
                    for (String str : yrmain.CustomHelp) {
                        player.sendMessage(str.replaceAll("&", "§"));
                    }
                }
            }
            return true;
        }
        return false;
    }
}