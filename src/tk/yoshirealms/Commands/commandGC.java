package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import tk.yoshirealms.yrmain;

/**
 * Created by dragontamerfred on 21-3-17.
 */
public class commandGC implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (yrmain.GCMessage != null) {
           /* String WorldLines = "";
            for (World world : Bukkit.getServer().getWorlds()) {
                WorldLines += main.GCWorldLine
                        .replaceAll("<WORLDNAME>", world.getName())
                        .replaceAll("<ENTITIES>", Integer.toString(world.getEntityCount()))
                        .replaceAll("<TILEENTITIES>", Integer.toString(world.getTileEntityCount()))
                        .replaceAll("<CHUNKS>", Integer.toString(world.getLoadedChunks().length))
                        + "\n";
            }
            for (String str : main.GCMessage) {
                str = str.replaceAll("&", "§")
                        .replaceAll("<UPTIME>", getUptime())
                        .replaceAll("<TPS>", Double.toString(Bukkit.getServer().getTPS()[0]))
                        .replaceAll("<MMemory>", Long.toString(Runtime.getRuntime().maxMemory()))
                        .replaceAll("<UMemory>", Long.toString(Runtime.getRuntime().maxMemory() - Runtime.getRuntime().freeMemory()))
                        .replaceAll("<WORLD>", WorldLines);
                sender.sendMessage(str);
            }*/
        }
        return true;
    }

    private static String getUptime() {
        long now = System.currentTimeMillis();
        long diff = now - yrmain.serverStartTime;

        String msg = (int)(diff / 86400000L) + " Days " + (int)(diff / 3600000L % 24L) + " Hours " + (int)(diff / 60000L % 60L) + " Minutes " + (int)(diff / 1000L % 60L) + " Seconds";
        return msg;
    }
}
