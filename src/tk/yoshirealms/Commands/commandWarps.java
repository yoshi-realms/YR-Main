package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 20/08/2017.
 * Project name: YR-Main
 */
public class commandWarps implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player p = (Player) commandSender;
        if (commandSender.hasPermission("yoshirealms.warps") && yrmain.WarpsOn) {
            if (commandSetWarp.Warps != null) {
                String msg = "";
            for (String i : commandSetWarp.Warps.keySet()) {

                msg += "§2" + i + "§a, ";
            }
                p.sendMessage(yrmain.BCPrefix + yrmain.AvailableWarps.replaceAll("<Warps>", msg));
        }
        else if (commandSetWarp.Warps == null) {
                p.sendMessage(yrmain.BCPrefix + yrmain.NoWarps);
            }
        } else if (!commandSender.hasPermission("yoshirealms.warps") && yrmain.WarpsOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        } else if (!yrmain.WarpsOn) {
            commandSender.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}