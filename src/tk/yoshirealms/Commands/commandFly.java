package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 16/01/2017.
 */
public class commandFly implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.fly") && yrmain.FlyOn) {
            if (strings.length == 0 && commandSender instanceof Player) {
                Player p = (Player) commandSender;
                if (p.getAllowFlight()) {
                    p.sendMessage(yrmain.BCPrefix + yrmain.FlyMsg);
                    p.setAllowFlight(false);
                    p.setFlying(false);
                } else {
                    p.sendMessage(yrmain.BCPrefix + yrmain.FlyMsg);
                    p.setAllowFlight(true);
                    p.setFlying(true);
                }


            } else if (strings.length == 1) {
                for (Player p : Bukkit.getServer().getOnlinePlayers()){
                    if (p.getName().equalsIgnoreCase(strings[0])) {
                        if (p.getAllowFlight()) {
                            p.sendMessage(yrmain.BCPrefix + yrmain.FlyMsg2.replaceAll("<PlayerName>" , commandSender.getName()));
                            p.setAllowFlight(false);
                            p.setFlying(false);
                            commandSender.sendMessage(yrmain.BCPrefix + yrmain.FlyRe.replaceAll("<PlayerName>" , strings[0]));
                        } else {
                            p.sendMessage(yrmain.BCPrefix + yrmain.FlyMsg2.replaceAll("<PlayerName>" , commandSender.getName()));
                            p.setAllowFlight(true);
                            p.setFlying(true);
                            commandSender.sendMessage(yrmain.BCPrefix + yrmain.FlyRe.replaceAll("<PlayerName>" , strings[0]));
                        }
                    }

                }
            }
            return true;
        } else if (yrmain.FlyOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        }

        else if (!yrmain.FlyOn && commandSender instanceof Player) {
            commandSender.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}
