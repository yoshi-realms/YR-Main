package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import tk.yoshirealms.yrmain;

import java.util.HashMap;

/**
 * Created by Asher on 09/07/2017.
 * Project name: YR-Main
 */
public class commandAnvil implements CommandExecutor {
    public static HashMap<Player, Inventory> AnvilItems = new HashMap<Player, Inventory>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.anvil") && yrmain.AnvilOn) {
            Player p = (Player)commandSender;
            p.closeInventory();
            p.openInventory(Bukkit.createInventory(p, InventoryType.ANVIL));
        } if (!commandSender.hasPermission("yoshirealms.anvil") && yrmain.InvseeOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);

        } else if (!yrmain.AnvilOn) {
            commandSender.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}