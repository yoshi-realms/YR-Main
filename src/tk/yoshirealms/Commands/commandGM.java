package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 18/01/2017.
 * Project name: YR-Main
 */
public class commandGM implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length == 1 && commandSender.hasPermission("yoshirealms.gm") && yrmain.GmOn) {
            Player p = (Player) commandSender;
            if ("0".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm0") || "s".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm0") || "survival".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm0")) {
                p.setGameMode(GameMode.SURVIVAL);
                p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg.replaceAll("<GameMode>", ("Survival")).replaceAll("&", "§"));
                return true;
            } else if ("1".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm1") || "c".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm1") || "creative".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm1")) {
                p.setGameMode(GameMode.CREATIVE);
                p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg.replaceAll("<GameMode>", ("Creative")).replaceAll("&", "§"));
                return true;
            } else if ("2".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm2") || "a".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm2") || "adventurer".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm2")) {
                p.setGameMode(GameMode.ADVENTURE);
                p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg.replaceAll("<GameMode>", ("Adventure")).replaceAll("&", "§"));
                return true;
            } else if ("3".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm3") || "sp".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm3") || "spectator".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm3")) {
                p.setGameMode(GameMode.SPECTATOR);
                p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg.replaceAll("<GameMode>", ("Spectator")).replaceAll("&", "§"));
                return true;
            }

        } else if (strings.length == 2 && commandSender.hasPermission("yoshirealms.gm") && yrmain.GmOn) {
            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                if (p.getName().equalsIgnoreCase(strings[1])) {
                    if ("0".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm0") || "s".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm0") || "survival".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm0")) {
                        p.setGameMode(GameMode.SURVIVAL);
                        p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg2.replaceAll("<GameMode>", ("Survival")).replaceAll("&", "§").replaceAll("<PlayerName>" , commandSender.getName()));
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.GmRe.replaceAll("<GameMode>", ("Survival")).replaceAll("&", "§").replaceAll("<PlayerName>", (strings[1])));
                        return true;
                    } else if ("1".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm1") || "c".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm1") || "creative".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm1")) {
                        p.setGameMode(GameMode.CREATIVE);
                        p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg2.replaceAll("<GameMode>", ("Creative")).replaceAll("&", "§").replaceAll("<PlayerName>" , commandSender.getName()));
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.GmRe.replaceAll("<GameMode>", ("Creative")).replaceAll("&", "§").replaceAll("<PlayerName>", (strings[1])));
                        return true;
                    } else if ("2".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm2") || "a".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm2") || "adventurer".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm2")) {
                        p.setGameMode(GameMode.ADVENTURE);
                        p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg2.replaceAll("<GameMode>", ("Adventure")).replaceAll("&", "§").replaceAll("<PlayerName>" , commandSender.getName()));
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.GmRe.replaceAll("<GameMode>", ("Adventure")).replaceAll("&", "§").replaceAll("<PlayerName>", (strings[1])));
                        return true;
                    } else if ("3".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm3") || "sp".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm3") || "spectator".equalsIgnoreCase(strings[0]) && p.hasPermission("yoshirealms.gm3")) {
                        p.setGameMode(GameMode.SPECTATOR);
                        p.sendMessage(yrmain.BCPrefix + yrmain.GmMsg2.replaceAll("<GameMode>", ("Spectator")).replaceAll("&", "§").replaceAll("<PlayerName>" , commandSender.getName()));
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.GmRe.replaceAll("<GameMode>", ("Spectator")).replaceAll("&", "§").replaceAll("<PlayerName>", (strings[1])));
                        return true;
                    }
                }
            }

        } else if (yrmain.GmOn && !(commandSender.hasPermission("yoshirealms.gm"))) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
            return true;
        } else if (!yrmain.GmOn) {
            commandSender.sendMessage(yrmain.HelpMsg);
        } else if (yrmain.GmOn && commandSender.hasPermission("yoshirealms.gm")) {
            commandSender.sendMessage("§cUsage: /gm (0/1/2/3) [Playername]");
        }
        return true;
    }
}
