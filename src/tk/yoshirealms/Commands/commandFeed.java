package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by decoo on 09/01/2017.
 */
public class commandFeed implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.feed") && yrmain.FeedOn) {
            if (strings.length == 1) {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (p.getName().equals(strings[0])) {
                        p.setFoodLevel(20);
                        p.setSaturation(10.0f);
                        p.sendMessage(yrmain.BCPrefix + yrmain.FeedMsg2.replaceAll("<PlayerName>" , commandSender.getName()));
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.FeedRe.replaceAll("<PlayerName>" , strings[0]));
                    }
                }
            } else if (commandSender instanceof Player) {
                Player player = (Player) commandSender;
                player.setFoodLevel(20);
                player.setSaturation(10.0f);
                player.sendMessage(yrmain.BCPrefix + yrmain.FeedMsg);
            }
        } else if (yrmain.FeedOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        } else if (!yrmain.FeedOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }
        return true;
    }
}
