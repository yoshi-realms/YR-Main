package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 07/07/2017.
 * Project name: YR-Main
 */
public class commandEc implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (commandSender.hasPermission("yoshirealms.ec") && yrmain.EcOn) {
                if (strings.length == 0) {
                    player.closeInventory();
                    player.openInventory(player.getEnderChest());
                } else if (strings.length == 1) {
                    if (!strings[0].contains("69")) {
                        if (strings.length == 1 && player.hasPermission("yoshirealms.ec-others")) {
                            Player p = Bukkit.getServer().getPlayer(strings[0]);
                            player.closeInventory();
                                player.openInventory(p.getPlayer().getEnderChest());

                        }
                    } else if (strings[0].contains("69")) {
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                    }
                    return true;
                }
            }
            return true;
        } else if (yrmain.EcOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        }

        else if (!yrmain.EcOn) {
            commandSender.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}