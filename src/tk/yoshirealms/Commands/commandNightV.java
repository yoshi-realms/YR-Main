package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 07/01/2017.
 */
public class commandNightV implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player && yrmain.NightVOn) {
            if (strings.length == 1) {
                if ("on".equalsIgnoreCase(strings[0])) {
                    if (commandSender.hasPermission("yoshirealms.nightvision")) {
                        Player player = (Player) commandSender;
                        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 1000000, 255 , false ,false));
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NVon);
                    } else {
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                    }
                    return true;
                } else if ("off".equalsIgnoreCase(strings[0])) {
                    if (commandSender.hasPermission("yoshirealms.nightvision")) {
                        Player player = (Player) commandSender;
                        player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NVoff);
                    } else {
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                    }
                    return true;
                }
            }
        }


        else if (!yrmain.NightVOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }
        if (yrmain.NightVOn) {
            commandSender.sendMessage("§cUsage: /nv (on/off)");
        }
        return true;
    }
}
