package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 07/07/2017.
 * Project name: YR-Main
 */
public class commandCraft implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
        Player player = (Player) commandSender;
        if (commandSender.hasPermission("yoshirealms.craft") && yrmain.CraftOn) {
            player.openWorkbench(null, true);
        }
        return true;
    } else if (yrmain.CraftOn) {
        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
    }

        else if (!yrmain.CraftOn) {
            commandSender.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}

