package tk.yoshirealms.Commands;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static tk.yoshirealms.yrmain.Storage;

/**
 * Created by Asher on 20/08/2017.
 * Project name: YR-Main
 */
public class commandSetWarp implements CommandExecutor {

    public static HashMap<String, Location> Warps = new HashMap<String , Location>();
    public static List<String> storeList = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player p = (Player) commandSender;
            if (commandSender.hasPermission("yoshirealms.setwarp") && yrmain.SetWarpOn) {
                if (args.length == 0 || args.length > 1) {
                    return false;
                }
                else if (args.length == 1) {
                    Warps.put(args[0] , p.getLocation());
                    p.sendMessage(yrmain.BCPrefix + yrmain.SetWarpSucces.replaceAll("<WarpName>", args[0]));

                    //De real shit van saven
                    for (String warp : Warps.keySet()) {
                        Location loc = Warps.get(warp);
                        World world = p.getLocation().getWorld();
                        String store = (int) loc.getX() + ";" + (int) loc.getY() + ";" + (int) loc.getZ() + ";" + world.getName() + ":" + warp;
                        storeList.add(store);
                    }
                    Storage.set("Warps", storeList);

                    return true;
                }
                else if (!commandSender.hasPermission("yoshirealms.setwarp") && yrmain.SetWarpOn) {
                    p.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                    return true;
                }
                else if (!yrmain.SetWarpOn) {
                    p.sendMessage(yrmain.BCPrefix + yrmain.HelpMsg);
                    return true;
                }
            }
        }
        return true;
    }
}
