package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 08/01/2017.
 */
public class commandSetSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.setspawn") && yrmain.SetSpawnOn) {
            if (commandSender instanceof Player) {
                Player p = (Player) commandSender;
                p.getWorld().setSpawnLocation(p.getLocation().getBlockX(), p.getLocation().getBlockY(), p.getLocation().getBlockZ());
                yrmain.SpawnWorld = p.getWorld().getName();
                commandSender.sendMessage(yrmain.BCPrefix + yrmain.SetSpawn);
            }
            return true;
        } else if (yrmain.SetSpawnOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        }

        else if (!yrmain.NightVOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }

        return true;
    }
}