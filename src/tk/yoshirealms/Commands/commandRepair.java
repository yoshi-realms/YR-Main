package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 09/07/2017.
 * Project name: YR-Main
 */
public class commandRepair implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.repair") && yrmain.RepairOn && commandSender instanceof Player) {
            Player p = (Player) commandSender;
            if (strings.length < 1 || strings[0].equalsIgnoreCase("hand")) {
                p.getItemInHand().setDurability((short) 0);
                return true;
            }
        }
        if (!commandSender.hasPermission("yoshirealms.repair") && yrmain.RepairOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);

        } else if (!yrmain.RepairOn) {
            commandSender.sendMessage(yrmain.HelpMsg);
        }
        return true;
    }
}