package tk.yoshirealms.Commands;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrmain;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Asher on 04/01/2017.
 */
public class commandBroodkast implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.yell") && yrmain.BroadCastOn) {
        List<String> BC = new ArrayList<>();
        for (String str : strings) {
            BC.add(str.replaceAll("&", "§"));
        }
        Bukkit.getServer().broadcastMessage(yrmain.BCPrefix + StringUtils.join(BC, " "));
        return true;
        } else if (yrmain.BroadCastOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        }
        else if (!yrmain.BroadCastOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }
        return true;
    }
}
