package tk.yoshirealms.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrmain;

import java.util.HashMap;

/**
 * Created by Asher on 08/01/2017.
 */
public class commandSpawn implements CommandExecutor, Listener {

    HashMap<String, Double> teleporting = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("yoshirealms.spawn") && yrmain.SpawnOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.BCPrefix + yrmain.TPCmessage);
                final Player player = (Player) commandSender;
                /*player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (yrmain.TPCooldown*20+20), 1 , false ,false));*/
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (yrmain.TPCooldown*20+2), 254 , false ,false));
                /*player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, (yrmain.TPCooldown*20+2), 250 , false ,false));
                player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, (yrmain.TPCooldown*20), -5 , false ,false));*/

                teleporting.put(player.getName(), player.getLocation().getY());
                Bukkit.getScheduler().scheduleSyncDelayedTask(yrmain.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (teleporting.containsKey(player.getName())) {
                            if (teleporting.get(player.getName()) == player.getLocation().getY()) {
                                player.teleport(Bukkit.getServer().getWorld(yrmain.SpawnWorld).getSpawnLocation());
                                player.sendMessage(yrmain.BCPrefix + yrmain.TPMessage);
                            }
                            else {
                                player.sendMessage(yrmain.BCPrefix + yrmain.TPCancelled);
                            }
                        }
                    }
                }, (yrmain.TPCooldown*20));
            }
            return true;
        } else if (yrmain.SpawnOn) {
            commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
        }

        else if (!yrmain.SpawnOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }

        return true;
    }
}