package tk.yoshirealms.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrmain;

/**
 * Created by Asher on 07/01/2017.
 */
public class commandHaste implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player && yrmain.HasteOn) {
            if (strings.length == 1) {
                if ("on".equalsIgnoreCase(strings[0])) {
                    if (commandSender.hasPermission("yoshirealms.haste")) {
                        Player player = (Player) commandSender;
                        player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 1000000, 255 , false ,false));
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.HasteTon);
                    } else {
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                    }
                    return true;
                } else if ("off".equalsIgnoreCase(strings[0])) {
                    if (commandSender.hasPermission("yoshirealms.haste")) {
                        Player player = (Player) commandSender;
                        player.removePotionEffect(PotionEffectType.FAST_DIGGING);
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.HasteToff);
                    } else {
                        commandSender.sendMessage(yrmain.BCPrefix + yrmain.NoPerms);
                    }
                    return true;
                }
            }
        }


        else if (!yrmain.HasteOn) {
            if (commandSender instanceof Player) {
                commandSender.sendMessage(yrmain.HelpMsg);
            }
        }
        if (yrmain.HasteOn) {
            commandSender.sendMessage("§cUsage: /nv (on/off)");
        }
        return true;
    }
}
